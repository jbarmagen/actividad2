fun main(){

    var hAna:Humano=Humano(21,"Ana",1.77)
    var hJulia:Humano=Humano(22,"Julia",1.8)
    var hYony:Humano?=null
    var iAnno:Int=2020

    hYony=Humano(36,"Yony",1.81)
    hYony!!.iEdad=36
    hYony.sNombre="Yony"
    hJulia.sNombre="Julia"

    hAna.iEdad=17
    hAna.sNombre="Ana"

    //println("Edad de Ana es: "+hAna.iEdad)
    //println("Ana puede conducir? "+hAna.es_apto_para_conducir())

    var ciclo:Int=0
    while(ciclo<30){ //Mientras el valor de ciclo<5
        //println("1) Ciclo es: "+ciclo+" al ser menor que 5 se cumple la condicion")
        //hAna.cumpleanos()
        hAna.desCumpleanos()
        hYony.desCumpleanos()
        hJulia.desCumpleanos()
        iAnno=iAnno-1
        ciclo=ciclo+1//actualizo el valor de ciclo a +1
        //println("2) el valor de ciclo es: "+ciclo)
        //println("3) Se acaba el While, y reviso si ciclo es menor que 5")
        //println()
    }
    println("En el año: "+iAnno+" las edades son: ")
    println("Edad de Ana es: "+hAna.iEdad)
    println("Edad de Yony es: "+hYony.iEdad)
    println("Edad de Julia es: "+hJulia.iEdad)
    //println("Ana puede conducir? "+hAna.es_apto_para_conducir())

}