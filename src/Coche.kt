class Coche {

    val dConsumoMedio:Double=5.5 //Consumo medio del coche por cada 100 kilomentros

    var sMarca:String="" //Marca del coche
    var sColor:String="" //Color del coche
    var sModelo:String=""
    var dLargo:Double=0.0
    var dAncho:Double=0.0
    var sTipoCombustible:String="" //Tipo de combustible, Gasolina, Diesel, Electrico, Hibrido
    var iNumPuertas:Int=0
    var dCombustible:Double=0.0
    var dKilometros:Double=0.0

    constructor(largo:Double,ancho:Double,tipoCombustible:String,numPuertas:Int){
        dLargo=largo
        dAncho=ancho
        sTipoCombustible=tipoCombustible
        iNumPuertas=numPuertas
    }

    /**
     * @author Yony BM
     * @param combustible: Variable que contiene la cantidad de combustible
     * que se quiera agregar al coche (en su variable dCombustible)
     * @sample coche.repostar(20.5) agregara 20.5 litros de combustible
     * @return no tiene return
     */
    fun repostar(combustible:Double){
        dCombustible=dCombustible+combustible
    }

    /**
     * @author Yony
     * @param distancia que recibe un double repesentando la disntacia EN KM
     * @return devuelve un TRUE si se pudo realizar el viaje, un FALSE si no por falta de combustible
     * @sample coche.realizarViaje(455)
     */
    fun realizarViaje(distancia:Double):Boolean{
        val dConsumoCombustible:Double=((distancia/100)*dConsumoMedio)
        if(dCombustible>=dConsumoCombustible){
            dKilometros=dKilometros+distancia
            dCombustible=dCombustible-dConsumoCombustible
            return true
        }
        else{
            return false
        }
    }

}