class Humano {

    var sPlaneta:String="Tierra"

    var iEdad:Int=0
    var sNombre:String?=null
    var dAltura:Double=0.0

    var cMiCoche:Coche?=null

    var arListaMisCoches= arrayListOf<Coche>()

    constructor(edad:Int,nombre:String,altura:Double){
        iEdad=edad
        sNombre=nombre
        dAltura=altura
        //println("ESTOY CONSTUYENDO UN OBJETO HUMANO A PARTIR DE LA CLASE HUMANO")
    }

    fun es_apto_para_conducir():Boolean{
        if(iEdad>=18){  //Si la edad de la CLASE ENTERA (HUMANA) es mayor o igual a 18, entonces devuelve VERDADERO
            return true
        }
        else{//Si es menor que 18 entonces devuelve FALSE
            return false
        }
    }

    fun cumpleanos(){
        var iTemp:Int=0
        iTemp=iEdad+1
        iEdad=iTemp
        //println("FELICIDADES!!!! "+sNombre)
    }

    fun desCumpleanos(){
        var iTemp:Int=0
        iTemp=iEdad-1
        iEdad=iTemp
        //println("DESFELICIDADES!!!! "+sNombre)
    }

}