fun main(){
    var hAna:Humano=Humano(21,"Ana",1.75)
    var hAngie:Humano=Humano(22,"Angie",1.76)
    var hGuillermo:Humano=Humano(20,"Guillermo",1.77)
    var hBryan:Humano=Humano(19,"Bryan",1.78)
    var hIgnacion:Humano=Humano(18,"Ignacio",1.79)

    var cClio:Coche=Coche(3.5,2.3,"Electrico",3)

    hAna.cMiCoche=cClio
    hAngie.cMiCoche=cClio

    hAna.cMiCoche!!.repostar(10.0)
    val isViajeAna=hAna.cMiCoche!!.realizarViaje(400.0)

    var isViajeAngie=hAngie.cMiCoche!!.realizarViaje(300.0)
    if(isViajeAngie==false){
        hAngie.cMiCoche!!.repostar(30.0)
        isViajeAngie=hAngie.cMiCoche!!.realizarViaje(300.0)
    }

    println("El viaje de Ana se pudo realizar? "+isViajeAna)
    println("El viaje de Angie se pudo realizar? "+isViajeAngie)

    println("La cantidad de Kilometros que tiene el Clio es: "+cClio.dKilometros)
}